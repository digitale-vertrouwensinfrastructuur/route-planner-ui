import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';

import React, { useState, useEffect } from "react";
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Button from 'react-bootstrap/Button';
import { Stage, Layer, Path, Image} from "react-konva";
import useImage from 'use-image';

function App() {
  /*global APPENV */
  const [image] = useImage('firetruck.png');

  const [error, setError] = useState(null);
  const [isLoaded, setIsLoaded] = useState(false);
  const [roadObstructionsAvailable, setRoadObstructionsAvailable] = useState(false);
  const [roadObstructions, setRoadObstructions] = useState([]);

  useEffect(() => {
    // Note: Expected json from the calculate-route endpoint should be something like: ([{"road":"Dagobertducklaan"},{"road":"Guusgelukparkweg"},{"road":"Katrienplein"},{"road":"Kwekkade"}])
    fetch("https://routeplanner.fire-brigade.dvi.app/api/v1/calculate-route")
    fetch(APPENV.ROAD_INTERRUPTION_SERVICE_URL)
      .then(res => res.json())
      .then(
        (result) => {
          console.log(result);
          setIsLoaded(true);
          setRoadObstructionsAvailable(true);
          setRoadObstructions(result);
        },
        (error) => {
          console.log(error);
          setIsLoaded(true);
          setRoadObstructionsAvailable(false);
          setError(error);
        }
      )
  }, [])

  return isLoaded ? (
    <Container>
      <Row style={{margin: "10px"}}>
        <Col md={8}><h1>Routeplanner</h1></Col>
        <Col md={4}><h1>DVI Demonstrator</h1></Col>
      </Row>
      <Row className="justify-content-md-center" style={{border: "1px solid black", margin: "10px"}}>
        <Col md={8}>
          <Stage width={620} height={874} style={{border: "1px solid black", width: "620px", margin: "10px"}}>
            <Layer>
              <Path data="M239.242,725.03L-3.007,856.94L-5.435,943.671L270.023,806.821L239.242,725.03Z" stroke="black" fill="rgb(156,192,253)" scaleX={0.5} scaleY={0.5} onMouseOver={() => console.log("hover")}/>
              <Path data="M307.744,702.901L330.75,777L814.986,690.356L812.488,608.99L307.744,702.901Z" stroke="black" fill="rgb(156,192,253)" scaleX={0.5} scaleY={0.5}/>

              <Path data="M332.879,776.9L496.171,1303.34L761.834,1269.14L576.842,734.24L332.879,776.9Z" stroke="black" fill="rgb(241,243,244)" scaleX={0.5} scaleY={0.5}/>
              <Path data="M287.428,876.8L425.709,1319.59L229.471,1392.26L98.334,975.105L287.428,876.8Z" stroke="black" fill="" scaleX={0.5} scaleY={0.5}/>
              <Path data="M-7.584,1030.95L33.599,1008.14L167.022,1411.3L-9.739,1501.75L-7.584,1030.95Z" stroke="black" fill="rgb(241,243,244)" scaleX={0.5} scaleY={0.5}/>
              <Path data="M438.275,1409.32L-12.268,1624.58L-14.703,1764.9L373.988,1766.62L438.275,1409.32Z" stroke="black" fill="rgb(241,243,244)" scaleX={0.5} scaleY={0.5}/>

              <Path data="M534.492,1398.46L483.536,1758.7L1275.91,1763.7L1287.68,1456.41L804.668,1356.14L534.492,1398.46Z" stroke="black" fill="rgb(193,228,202)" scaleX={0.5} scaleY={0.5}/>

              <Path data="M1269.34,1313.08L849.766,1260.88L810.276,1169.49L1261.18,1205.4L1269.34,1313.08Z" stroke="black" fill="rgb(241,243,244)" scaleX={0.5} scaleY={0.5}/>
              <Path data="M784.708,1088.44L755.26,994.206L964.573,1002.73L995.802,1109.78L784.708,1088.44Z" stroke="black" fill="rgb(241,243,244)" scaleX={0.5} scaleY={0.5}/>
              <Path data="M1090.11,1108.15L1274.4,1119.57L1287.92,995.924L1069.73,1005.41L1090.11,1108.15Z" stroke="black" fill="rgb(241,243,244)" scaleX={0.5} scaleY={0.5}/>
              <Path data="M708.525,788.395L745.571,914.642L1248.86,917.622L1281.47,769.009L826.689,768.664L708.525,788.395Z" stroke="black" fill="rgb(241,243,244)" scaleX={0.5} scaleY={0.5}/>

              <Path data="M885.948,607.463L884.417,688.146L1272.7,687.775L1273.29,582.449L885.948,607.463Z" stroke="black" fill="rgb(156,192,253)" scaleX={0.5} scaleY={0.5}/>

              <Path data="M360.213,609.505L276.84,347.75L410.975,314.207L490.971,584.59L360.213,609.505Z" stroke="black" fill="rgb(241,243,244)" scaleX={0.5} scaleY={0.5}/>
              <Path data="M598.078,563.974L736.983,548.091L681.639,240.242L537.192,270.406L598.078,563.974Z" stroke="black" fill="rgb(241,243,244)" scaleX={0.5} scaleY={0.5}/>

              <Path data="M-32.607,759.249L272.21,616.162L197.671,358.723L-11.901,379.141L-32.607,759.249Z" stroke="black" fill="rgb(147,205,158)" scaleX={0.5} scaleY={0.5}/>

              <Path data="M-25.033,299.244L-26.537,380.334L198.122,356.474L188.532,272.553L-25.033,299.244Z" stroke="black" fill="rgb(156,192,253)" scaleX={0.5} scaleY={0.5}/>
              <Path data="M278.041,343.975L261.428,253.992L814.501,147.26L831.75,218.285L540.124,267.484L409.385,313.571L278.041,343.975Z" stroke="black" fill="rgb(156,192,253)" scaleX={0.5} scaleY={0.5}/>
              <Path data="M921.905,130.828L933.006,212.433L1277.08,175.896L1285.09,77.122L921.905,130.828Z" stroke="black" fill="rgb(156,192,253)" scaleX={0.5} scaleY={0.5}/>

              <Path data="M-22.027,202.113L633.976,118.155L589.253,-18.299L-12.799,-15.594L-22.027,202.113Z" stroke="black" fill="rgb(241,243,244)" scaleX={0.5} scaleY={0.5}/>
              <Path data="M738.68,101.544L695.214,-22.729L1261.7,-28.49L1279.24,18.582L738.68,101.544Z" stroke="black" fill="rgb(241,243,244)" scaleX={0.5} scaleY={0.5}/>
              
              {/* Circles at bridges */}
              
              {/* Roadblocks */}
              {roadObstructionsAvailable && (
                <Path data="M636.19,119.984L646.247,175.689L759.195,151.443L736.332,99.166L636.19,119.984Z" stroke="black" fill="rgb(211,33,49)" scaleX={0.5} scaleY={0.5}/>
              )}

              {roadObstructionsAvailable && (
                <Path data="M373.216,1136.8L435.11,1115.04L481.135,1261.66L416.13,1277.26L373.216,1136.8Z" stroke="black" fill="rgb(211,33,49)" scaleX={0.5} scaleY={0.5}/>
              )}

              {roadObstructionsAvailable && (
                <Path data="M671.875,997.639L762.302,1255.73L846.637,1255.58L779.968,1090.15L753.584,992.982L671.875,997.639Z" stroke="black" fill="rgb(211,33,49)" scaleX={0.5} scaleY={0.5}/>
              )}

              {/* Triangles */}
              <Path data="M537.354,135.921L576.871,54.272L500.535,58.494L537.354,135.921Z" stroke="black" fill="rgb(211,161,0)" scaleX={0.5} scaleY={0.5}/>
              <Path data="M537.354,135.921L576.871,54.272L500.535,58.494L537.354,135.921Z" stroke="black" fill="rgb(211,161,0)" scaleX={0.5} scaleY={0.5} x={-80} y={670}/>

              {/* Routes */}
              {!roadObstructionsAvailable && (
                <Path data="M985.921,408.817L912.15,411.05L878.267,186.531L866.936,113.512L799.143,120.64L567.071,153.581" strokeWidth={6} dash={[15,5]} dashEnabled="true" stroke="black" scaleX={0.5} scaleY={0.5}/>
              )}
              
              {roadObstructionsAvailable && (
                <Path data="M988.599,422.455L769.362,457.445L773.938,575.745L514.305,610.505L320.465,635.339L255.803,384.512L228.637,230.45L408.149,186.174L546.058,155.116" strokeWidth={6} dash={[15,5]} dashEnabled="true" stroke="black" scaleX={0.5} scaleY={0.5}/>
              )}

              {roadObstructionsAvailable && (
                <Path data="M996.403,444.954L819.817,477.16L787.064,583.983L641.454,612.693L351.967,657.551L277.698,675.488L305.628,834.995L117.73,927.623L52.085,969.811L113.269,1173.35L215.604,1451.16L351.159,1403.38" strokeWidth={6} dash={[15,5]} dashEnabled="true" stroke="black" scaleX={0.5} scaleY={0.5}/>
              )}
              {!roadObstructionsAvailable && (
                <Path data="M997.408,480.713L854.991,527.682L849.733,627.154L843.415,743.366L659.948,784.848L821.427,1315.55L622.489,1330.64L457.153,1376.32" strokeWidth={6} dash={[15,5]} dashEnabled="true" stroke="black" scaleX={0.5} scaleY={0.5}/>
              )}
            </Layer>
            <Layer>
              <Image image={image} x={480} y={140} width={150} height={100}/>  
            </Layer>
          </Stage>
        </Col>
        <Col md={4} style={{fontSize: "34px"}}>
          <br/>
          <h3>Wegopbrekingen:</h3>
          {roadObstructionsAvailable ? (
          <ul>
            {roadObstructions.map(ro => (
              <li key={ro.id}>
                {ro.road}
              </li>
            ))}
          </ul>
          ) : (
            <span style={{color: "red"}}>Informatie over wegopbrekingen niet beschikbaar (geen toegang)</span>
          )}
          <br/><br/>
          <Button onClick={() => {window.location.reload();}}>Verversen</Button>
        </Col>
      </Row>
      <Row style={{margin: "10px", color: "white"}}>Error: {error ? error.toString() : ""}</Row>
    </Container>
  ) : ("Bezig met laden...");
}

export default App;
